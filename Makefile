.PHONY: all clean

CC = clang
CFLAGS = -std=c90 -Weverything
LDFLAGS = -lmrss

all : rssd

rssd : rssd.c rssd.h
	$(CC) $(CFLAGS) rssd.c $(LDFLAGS) -o rssd

clean :
	rm -f rssd

