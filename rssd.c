#include "rssd.h"

int main(void)
{
    mrss_t *feed = malloc(sizeof(mrss_t));
    mrss_error_t err = 0;
    char url[MAX_URL_LENGTH] = "";

    while (fgets(url, MAX_URL_LENGTH, stdin) != NULL) {
        chomp(url);
        fprintf(stderr, "Processing URL: %s\n", url);
        err = mrss_parse_url(url, &feed);
        fprintf(stderr, "Error value: %u\n", err);
        if (err == 0) {
            fprintf(stderr, "Feed title: %s\n", feed->title);
            fprintf(stderr, "Feed description: %s\n", feed->description);
        }
    }

    exit(EXIT_SUCCESS);
}

void chomp(char *s) {
    s[strcspn(s, "\n")] = 0;
    return;
}
