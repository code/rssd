rssd
====

All this does is attempt to retrieve all your feeds and print their titles and
descriptions at the moment. But it works!

    $ sudo apt-get install libmrss0 libmrss0-dev
    $ make
    $ ./rssd < ~/.config/newsbeuter/urls

License
-------

Copyright (c) [Tom Ryder][1]. Distributed under an [MIT License][2].

[1]: https://sanctum.geek.nz/
[2]: https://www.opensource.org/licenses/MIT
